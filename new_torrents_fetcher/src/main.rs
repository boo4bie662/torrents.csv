extern crate clap;
extern crate csv;
extern crate reqwest;
extern crate select;

use clap::{App, Arg};
use select::document::Document;
use select::predicate::{Attr, Class, Name, Predicate};
use std::fs;
use std::path::Path;
use std::process::Command;
use std::{thread, time};
use std::io::{BufRead, BufReader};
use std::fs::File;

static mut COOKIE: &str = "";
static mut USER_AGENT: &str = "";

fn main() {
  let matches = App::new("New Torrents Fetcher")
    .version("0.1.0")
    .author("Dessalines")
    .about("Fetches new torrent files from various sites.")
    .arg(
      Arg::with_name("TORRENT_SAVE_DIR")
      .short("s")
      .long("save_dir")
      .value_name("DIR")
      .takes_value(true)
      .help("Where to save the torrent files.")
      .required(true),
      )
    .arg(
      Arg::with_name("FETCH_SITES")
      .short("fs")
      .long("fetch_sites")
      .help("Fetches from various torrent sites.")
      )
    .arg(
      Arg::with_name("HASH_FILE")
      .short("hf")
      .long("hash_file")
      .value_name("FILE")
      .takes_value(true)
      .help("The location of a file containing rows of infohashes. If given, it will download those infohashes."),
      )
    .arg(
      Arg::with_name("TORRENTS_CSV_FILE")
      .short("t")
      .long("torrents_csv")
      .value_name("FILE")
      .takes_value(true)
      .help("The location of a torrents.csv file. If given, it will download those infohashes."),
      )
    .get_matches();

  let save_dir = Path::new(matches.value_of("TORRENT_SAVE_DIR").unwrap());

  fetch_cloudflare_cookie();

  if matches.is_present("FETCH_SITES") {
    // torrentz2(save_dir);
    thepiratebay(save_dir);
    magnetdl(save_dir);
    leetx(save_dir);
    skytorrents(save_dir);
  }

  if let Some(t) = matches.value_of("HASH_FILE") {
    hash_file_scan(Path::new(t), save_dir);
  }

  if let Some(t) = matches.value_of("TORRENTS_CSV_FILE") {
    torrents_csv_scan(Path::new(t), save_dir);
  }
}

fn hash_file_scan(hash_file: &Path, save_dir: &Path) {
  let f = File::open(&hash_file).expect("Unable to open file");
  let f = BufReader::new(f);
  for line in f.lines() {
    let hash = line.expect("Unable to read line");
    fetch_torrent(hash, save_dir);
  }
}

fn torrents_csv_scan(torrents_csv_file: &Path, save_dir: &Path) {
  for hash in collect_info_hashes(torrents_csv_file) {
    fetch_torrent(hash, save_dir);
  }
}

fn collect_info_hashes(torrents_csv_file: &Path) -> Vec<String> {
  println!("Scanning torrent infohashes...");
  let mut rdr = csv::ReaderBuilder::new()
    .has_headers(false)
    .delimiter(b';')
    .from_path(torrents_csv_file)
    .unwrap();
  rdr.records().map(|x| x.unwrap()[0].to_string()).collect()
}

fn torrentz2(save_dir: &Path) {
  // https://torrentz2.eu/search?f=&p=19

  let page_limit = 19;

  let base_url = "https://torrentz2.eu";

  let mut pages: Vec<String> = Vec::new();

  let types = [
    "application",
    "tv",
    "movie",
    "adult",
    "music",
    "mp3",
    "anime",
    "game",
    "ebook",
    "adult",
    "x265",
    "hevc",
    "yify",
    "discography",
  ];
  for c_type in types.iter() {
    for i in 0..page_limit {
      let page = format!("{}/search?f={}&p={}", base_url, c_type, i);
      pages.push(page);
    }
  }

  for page in pages.iter() {
    println!("Fetching page {}", page);
    let html = match fetch_html(page) {
      Ok(t) => t,
      _err => continue,
    };

    let document = Document::from(&html[..]);
    println!("This is weird am I'm not sure about any of this");

    for row in document.find(Name("dt").descendant(Name("a"))) {
      let hash = match row.attr("href") {
        Some(t) => t.to_string(),
        None => continue,
      };
      println!("{}", &hash);
      fetch_torrent(hash, save_dir);
    }
  }
}

fn magnetdl(save_dir: &Path) {
  let page_limit = 30;

  let base_url = "https://magnetdl.com";

  let mut pages: Vec<String> = Vec::new();

  let types = ["software", "movies", "games", "e-books", "tv", "music"];
  // https://www.magnetdl.com/download/software/se/desc/1/
  for c_type in types.iter() {
    for i in 1..page_limit {
      let page = format!("{}/download/{}/se/desc/{}/", base_url, c_type, i);
      pages.push(page);
    }
  }

  for page in pages.iter() {
    println!("Fetching page {}", page);
    let html = match fetch_html(page) {
      Ok(t) => t,
      _err => continue,
    };

    let document = Document::from(&html[..]);

    for row in document.find(Class("m").descendant(Name("a"))) {
      let hash = match row.attr("href") {
        Some(t) => t.to_string().chars().skip(20).take(40).collect(),
        None => continue,
      };
      fetch_torrent(hash, save_dir);
    }
  }
}

fn thepiratebay(save_dir: &Path) {
  let page_limit = 35;

  let base_url = "https://thepiratebay.org/browse";

  let mut pages: Vec<String> = Vec::new();

  let types = [
    "100", "101", "102", "103", "104", "199",
    "200", "201", "202", "203", "204", "205", "206", "207", "208", "209", "299",
    "300", "301", "302", "303", "304", "305", "306", "399",
    "400", "401", "402", "403", "404", "405", "406", "407", "408", "499",
    "500", "501", "502", "503", "504", "505", "506", "599",
    "600", "601", "602", "603", "604", "605", "699"
  ];
  for c_type in types.iter() {
    for i in 0..page_limit-1 {
      let page = format!("{}/{}/{}/7", base_url, c_type, i);
      pages.push(page);
    }
  }

  for page in pages.iter() {
    println!("Fetching page {}", page);

    let html = match fetch_html(page) {
      Ok(t) => t,
      _err => continue,
    };
    let document = Document::from(&html[..]);
    for row in document.find(Attr("id", "searchResult").descendant(Name("tbody")).descendant(Name("tr"))) {
      let hash_td = match row.find(Name("td").descendant(Name("a"))).nth(3) {
        Some(t) => t,
        None => continue,
      };
      let hash = match hash_td.attr("href") {
        Some(t) => t.chars().skip(20).take(40).collect(),
        None => continue,
      };

      fetch_torrent(hash, save_dir);
    }
  }
}


fn skytorrents(save_dir: &Path) {
  let page_limit = 100;

  let base_url = "https://www.skytorrents.lol";

  let mut pages: Vec<String> = Vec::new();

  for i in 1..page_limit {
    let page = format!("{}/top100?page={}", base_url, i);
    pages.push(page);
  }

  let types = [
    "video", "audio", "games", "software", "doc", "ebook", "yify", "epub", "xxx", "show", "album",
    "1080",
  ];
  for c_type in types.iter() {
    for i in 1..page_limit {
      let page = format!("{}/top100?type={}&page={}", base_url, c_type, i);
      pages.push(page);
    }
  }

  for page in pages.iter() {
    println!("Fetching page {}", page);

    let html = match fetch_html(page) {
      Ok(t) => t,
      _err => continue,
    };
    let document = Document::from(&html[..]);
    for row in document.find(Attr("id", "results").descendant(Name("tr"))) {
      let hash_td = match row.find(Name("td").descendant(Name("a"))).nth(2) {
        Some(t) => t,
        None => continue,
      };
      let hash = match hash_td.attr("href") {
        Some(t) => t.chars().skip(20).take(40).collect(),
        None => continue,
      };

      fetch_torrent(hash, save_dir);
    }
  }
}

fn leetx(save_dir: &Path) {
  let page_limit = 50;

  let base_url = "https://1337x.to";

  let mut pages: Vec<String> = Vec::new();

  let types = [
    "Games",
    "Anime",
    "Apps",
    "Documentaries",
    "Movies",
    "Music",
    "Other",
    "TV",
    "XXX",
  ];

  for i in 1..page_limit {
    for c_type in types.iter() {
      let page = format!("{}/sort-cat/{}/seeders/desc/{}/", base_url, c_type, i);
      pages.push(page);
    }
  }

  for page in pages.iter() {
    println!("Fetching page {}", page);
    let html = match fetch_html(page) {
      Ok(t) => t,
      _err => continue,
    };
    let document = Document::from(&html[..]);

    for row in document.find(
      Class("table-list")
      .descendant(Name("tbody"))
      .descendant(Name("tr")),
      ) {
      let detail_page_url_col = match row.find(Class("coll-1")).nth(0) {
        Some(t) => t,
        None => continue,
      };
      let detail_page_url_name = match detail_page_url_col.find(Name("a")).nth(1) {
        Some(t) => t,
        None => continue,
      };
      let detail_page_url_href = match detail_page_url_name.attr("href") {
        Some(t) => t,
        None => continue,
      };

      let detail_full_url = format!("{}{}", base_url, detail_page_url_href);
      println!("Fetching page {}", detail_full_url);
      let detail_html = match fetch_html(&detail_full_url) {
        Ok(t) => t,
        _err => continue,
      };

      let detail_document = Document::from(&detail_html[..]);
      let hash = match detail_document
        .find(Class("infohash-box").descendant(Name("span")))
        .nth(0)
        {
          Some(t) => t.text().to_lowercase(),
          None => continue,
        };

      fetch_torrent(hash, save_dir);
    }
  }
}

fn fetch_torrent(hash: String, save_dir: &Path) {
  // Curl is the only thing that works with itorrent
  let file_name = format!("{}.torrent", hash);
  let url = format!(
    "https://itorrents.org/torrent/{}.torrent",
    &hash.to_ascii_uppercase()
    );

  let full_path = save_dir
    .join(&file_name)
    .into_os_string()
    .into_string()
    .unwrap();

  if !Path::new(&full_path).exists() {
    unsafe {
      Command::new("curl")
        .args(&[
              &url,
              "-H",
              USER_AGENT,
              "-H",
              COOKIE,
              "--compressed",
              "-o",
              &full_path,
              "-s",
        ])
        .output()
        .expect("curl command failed");
      check_cloud_flare(Path::new(&full_path));
      thread::sleep(time::Duration::from_millis(2742));
      println!("{} saved.", &full_path);
    }
  }
}

fn check_cloud_flare(file: &Path) {
  let data = match fs::read_to_string(file) {
    Ok(t) => t,
    _err => return,
  };

  if data == "" {
    return;
  }

  let first_line = &data[..5];

  if first_line == "<!DOC" {
    fs::remove_file(file);
    println!("Cloudflare failed, re-fetching.");
    fetch_cloudflare_cookie();
  }
}

fn fetch_cloudflare_cookie() {
  unsafe {
    println!("Fetching new CloudFlare Cookie...");
    let output = Command::new("python")
      .args(&["src/cf.py"])
      .output()
      .expect("python command failed");

    let out = string_to_static_str(format!("{}", String::from_utf8_lossy(&output.stdout)));
    let split: Vec<&str> = out.lines().collect();
    COOKIE = split[1];
    USER_AGENT = split[2];
  }
}

fn string_to_static_str(s: String) -> &'static str {
  Box::leak(s.into_boxed_str())
}

fn fetch_html(url: &str) -> Result<String, reqwest::Error> {
  reqwest::Client::new()
    .get(url)
    .header("Accept", "text/html")
    .send()?
    .text()
}
